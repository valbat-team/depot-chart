{{/*
Expand the name of the chart.
*/}}
{{- define "projet.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{- define "webservice.name" -}}
{{- printf "%s-%s" (include "projet.name" .) .Values.webservice.name | trunc 63 | trimSuffix "-" -}}
{{- end -}}


{{- define "app.name" -}}
{{- printf "%s-%s" (include "projet.name" .) .Values.app.name | trunc 63 | trimSuffix "-" -}}
{{- end -}}


{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "projet.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{- define "webservice.fullname" -}}
{{- printf "%s-%s" (include "projet.fullname" .) .Values.webservice.name | trunc 63 | trimSuffix "-" -}}
{{- end -}}


{{- define "app.fullname" -}}
{{- printf "%s-%s" (include "projet.fullname" .) .Values.app.name | trunc 63 | trimSuffix "-" -}}
{{- end -}}


{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "projet.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{- define "webservice.chart" -}}
{{- printf "webservice" -}}
{{- end -}}

{{- define "app.chart" -}}
{{- printf "app" -}}
{{- end -}}

{{/*
Common labels
*/}}
{{- define "projet.labels" -}}
helm.sh/chart: {{ include "projet.chart" . }}
{{ include "projet.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{- define "webservice.labels" -}}
helm.sh/chart: {{ include "webservice.chart" . }}
{{ include "webservice.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end -}}

{{- define "app.labels" -}}
helm.sh/chart: {{ include "app.chart" . }}
{{ include "app.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end -}}

{{/*
Selector labels
*/}}
{{- define "projet.selectorLabels" -}}
app.kubernetes.io/name: {{ include "projet.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{- define "webservice.selectorLabels" -}}
app.kubernetes.io/name: {{ include "webservice.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end -}}

{{- define "app.selectorLabels" -}}
app.kubernetes.io/name: {{ include "app.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end -}}

{{/*
Create the name of the service account to use
*/}}
{{- define "app.serviceAccountName" -}}
{{- if .Values.app.serviceAccount.create -}}
    {{ default (include "app.fullname" .) .Values.app.serviceAccount.name }}
{{- else -}}
    {{ default "default" .Values.app.serviceAccount.name }}
{{- end -}}
{{- end -}}

{{- define "webservice.serviceAccountName" -}}
{{- if .Values.webservice.serviceAccount.create -}}
    {{ default (include "webservice.fullname" .) .Values.webservice.serviceAccount.name }}
{{- else -}}
    {{ default "default" .Values.webservice.serviceAccount.name }}
{{- end -}}
{{- end -}}